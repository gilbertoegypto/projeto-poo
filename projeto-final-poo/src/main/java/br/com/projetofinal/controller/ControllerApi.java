package br.com.projetofinal.controller;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.projetofinal.entidades.Funcionario;
import br.com.projetofinal.servico.FuncionarioDao;

@Controller
public class ControllerApi {
	
	@Autowired
	private FuncionarioDao servico;
	
	@RequestMapping("/")
	public String paginaInicial(Model model) {
		List<Funcionario> funcionarios = servico.listarTodosFuncionarios();
		funcionarios.sort(Comparator.comparingLong(Funcionario::getId));
		model.addAttribute("listafuncionarios",funcionarios);
		return "index";
	}
	
	@RequestMapping("/novo")
	public String novoProduto(Model model) {
		Funcionario funcionario = new Funcionario();
		model.addAttribute("funcionario",funcionario);
		
		return "cadastrar";
	}
	
	@RequestMapping(value="/salvar", method=RequestMethod.POST)
	public String salvar(@ModelAttribute("funcionario") Funcionario funcionario) {
		servico.salvarFuncionario(funcionario);
		return "redirect:/";
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView paginaEditar(@PathVariable(name="id")long id) {
		
		Funcionario funcionario = servico.modificarFuncionario(id);
		
		ModelAndView mav = new ModelAndView("editarFuncionario");
		
		mav.addObject("funcionario",funcionario);
		
		return mav;
	}
	
	@RequestMapping("/deletar/{id}")
	public String deletar(@PathVariable(name="id") long id) {
		
		servico.deletarFuncionario(id);
		return "redirect:/";
	}
	
}