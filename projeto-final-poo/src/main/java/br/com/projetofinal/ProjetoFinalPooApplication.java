package br.com.projetofinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoFinalPooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoFinalPooApplication.class, args);
	}

}
