package br.com.projetofinal.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Funcionario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nome;
	private String cargo;
	private int quantidadeDependentes;
	private double salario;
	double salarioFinal;
	
	public Funcionario() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public int getQuantidadeDependentes() {
		return quantidadeDependentes;
	}

    public void setQuantidadeDependentes(int quantidadeDependentes) {
        if (quantidadeDependentes <0){
          throw new IllegalArgumentException("Quantidade minima de Dependentes é zero");

        }
        
          this.quantidadeDependentes = quantidadeDependentes;
      }

    public double getSalario() {
    	salarioFinal = (salario*quantidadeDependentes/50)+salario;
    return salarioFinal;	
}

    public void setSalario(double salario) {
    	if (salario <0){
    		throw new IllegalArgumentException("O salaraio deve ser maior que zero");

    	}
    		this.salario = salario;
    }
    
    public double getSalarioFinal() {
    	salarioFinal = (salario*quantidadeDependentes/50)+salario;
    return salarioFinal;
}
   /* public double earning() {
    	//return getQuantidadeDependentes()/50;
    	double taxa = 0.02;
    	double taxaFinal = taxa*quantidadeDependentes;
    	salario = (salario*taxaFinal)+salario;
    	return salario;
    }*/

}
