package br.com.projetofinal.servico;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projetofinal.entidades.Funcionario;
import br.com.projetofinal.repositorio.FuncionarioRepositorio;
@Service
public class FuncionarioDao {
	
	
	@Autowired
	FuncionarioRepositorio repo;
	
	public void salvarFuncionario(Funcionario funcionario) {
		repo.save(funcionario);
	}

	/*public void adicionarFuncionario(Funcionario funcionario) {
		repo.save(funcionario);
	}*/
	
	public List<Funcionario> listarTodosFuncionarios() {
		
		List<Funcionario> funcionario = repo.findAll();
		/*
		for(Funcionario l : funcionario) {
			System.out.println(l.getNome());
		}*/
		
		return repo.findAll();
		
	}
	
	public Funcionario modificarFuncionario(Long id) {
		return repo.findById(id).get();
	}
	
	public void deletarFuncionario(Long id) {
		repo.deleteById(id);
	}
}
