package br.com.projetofinal.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.projetofinal.entidades.Funcionario;
@Repository
public interface FuncionarioRepositorio extends JpaRepository<Funcionario, Long> {
	

}
